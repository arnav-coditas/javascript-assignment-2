const userethods={
    about : function(){
        return `${this.firstName} is ${this.age} years and ${this.address} is his adress`
    }
}


function createUser(firstName, lastname,email,age,address){
    const user={};
    user.firstName = firstName;
    user.lastName=lastName;
    user.email=email;
    user.address= address;
    user.about= userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}

const user1= createUser('Arnav','patil','arnav.patil@coditas.com','676 rc nagar belgaum');
const user2= createUser('Rushal','Sarthak','rushal.al@coditas.com','676 rc nagar belgaum');
const user3= createUser('Zaedan','B','zaedan.B@coditas.com','676 rc nagar belgaum');
console.log(user1.about());
console.log(user3.about());
