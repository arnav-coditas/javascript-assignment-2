// object destructuring

const band ={
    bandName:"zaedan band",
    famousSong: "stairway to heaven",
    year: 2000,
    anotherFamousSong: "above sky"

};
// const {bandName, famousSong}= band;
// bandName="queen"
// console.log(bandName);

let{ bandName, famousSong, ...restProps}= band;
console.log(bandName);
console.log(restProps);


