
//push array pop array

let fruits= ["apple" , "mango", "grapes"];
//push
console.log(fruits);
fruits.push("custard apple");
console.log(fruits);
let poppedFruit = fruits.pop();

console.log(poppedFruit);


fruits.unshift("banana");
fruits.unshift("kiwi");
console.log(fruits);

let removedFruit = fruits.shift();
console.log(fruits);
console.log("removed fruit is", removedFruit);