function hi(){
    console.log("hi, welcome");
}
//fun+obj
console.log(hi.name());
//we can add out properties
hello.myOwnprop= "I am an Engineer";
console.log(hi.myOwnprop);

//function gives us free space and thats called prototype

console.log(hi.prototype);


if(hi.prototype){
    console.log("Prototype present");
}else{
    console.log("prototype not present");
}


// hi.prototype.xyz="ace";

// console.log(hi.prototype);
// hi.prototype.sing= functions(){
//     return "value is returned";
// };
// console.log(hi.prototype.sing());
