//lexical scope sec
// const myvar= "value1";
// function myApp(){
    
//     function myfun(){
//       //const myvar= "value77";

//         console.log("inside myfunc", myvar);
//     }
//     const myfun2 =function(){}
//     const myfunc3 =() =>{}
//     console.log(myvar);
//     myfun();
// }
// myApp();

const myvar= "value1";
function myApp(){
    
    function myfun2()=> {
      //const myvar= "value77";
      
        console.log("inside myfunc", myvar);
    }
    myfun2();
}
    console.log(myvar);
    myfun();
}

myApp();