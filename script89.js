//instance/object



class Dog{
    constructor(name,age){
        this.name= name;
        this.age= age;
    }
    sit(){
        return `${this.name} is sitting`;
    }
    isSuperActive(){
        return this.age<=1;
    }

    isCute(){
        return true;
    }

}
const rocky = new Dog("rocky",3);
console.log(tommy.isCute());


class Dog extends animal{
    constructor(name,age,speed){
        super(name,age);
        this.speed=speed;
    }

    run(){
        return `${this.name} is running at ${this.speed}`
    }
    
}

const tommy = new Dog("tommy", 3,45);
console.log(tommy.run());

console.log(tommy.sit());



const animal1 = new Animal('rocky',2);
console.log(animal1.sit());