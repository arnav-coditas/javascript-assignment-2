
// while loop example
let num = 10;
let sum=0;
let i=0;
while(i<=10){
    
sum= sum+ i;
i++;
}
console.log(sum);


// this is much faster as it is taking linear time
let total1 = (num*(num+1))/2;
console.log(total1);