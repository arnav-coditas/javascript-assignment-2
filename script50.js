//block scope

//vs
//functionscope --> var
//const --> cant accesss outside the scope

// {
//     var firstName = "Arnav";
//     console.log(firstName);
// }

// {
// console.log(firstName);
// }


// if(true){
//     let firstName =" Arnav";
//     console.log(firstName);
// }

console.log(firstName);

function myapp(){
if(true){
    var firstName = "Arnav";
    console.log(firstName);
}

console.log(firstName);
}

myapp();
